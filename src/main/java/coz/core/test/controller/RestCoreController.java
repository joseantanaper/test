/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coz.core.test.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author omnizeus
 *
 * localhost:port/test/api
 *
 */
@RestController
@RequestMapping(value = "/api")
public class RestCoreController {

    private static final Logger logger = LogManager.getLogger(RestController.class);

    /**
     * localhost:port/test/api/
     *
     * @return
     */
    @RequestMapping(value = {"", "/", "welcome"})
    public String api() {
        return "/api/";
    }

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String test() {
        //Logger not showing in NetBeans console!!!
        logger.fatal("HELLO FATAL TEST");
        logger.error("HELLO ERROR TEST");
        logger.warn("HELLO WARN TEST");
        logger.trace("HELLO TRACE TEST");
        logger.info("HELLO INFO TEST");
        logger.debug("HELLO DEBUG TEST");
        return "hello";
    }

}
