/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coz.core.test.controller;

import java.security.Principal;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author omnizeus
 *
 * localhost:port/test localhost:port/test/
 */
@Controller
public class CoreController {

    private static final Logger logger = LogManager.getLogger(RestController.class);

    @RequestMapping(value = {"", "/"})
    @ResponseBody
    public String root(Model model, Principal principal, HttpServletRequest request, HttpServletResponse response) {
        logger.fatal("ROOT FATAL TEST");
        logger.error("ROOT ERROR TEST");
        logger.warn("ROOT WARN TEST");
        logger.trace("ROOT TRACE TEST");
        logger.info("ROOT INFO TEST");
        logger.debug("ROOT DEBUG TEST");
        return "/";
    }

    //TODO: BasicErrorController

}
